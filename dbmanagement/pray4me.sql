-- MySQL dump 10.13  Distrib 8.0.22, for Linux (x86_64)
--
-- Host: localhost    Database: pray4me
-- ------------------------------------------------------
-- Server version	8.0.22-0ubuntu0.20.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Campus`
--

DROP TABLE IF EXISTS `Campus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Campus` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Campus`
--

LOCK TABLES `Campus` WRITE;
/*!40000 ALTER TABLE `Campus` DISABLE KEYS */;
INSERT INTO `Campus` VALUES (1,'East'),(2,'Northeast'),(3,'West'),(4,'Chihuahua'),(5,'San Antonio');
/*!40000 ALTER TABLE `Campus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Comentarios`
--

DROP TABLE IF EXISTS `Comentarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Comentarios` (
  `id` bigint NOT NULL,
  `creador` tinyblob,
  `fecha` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `mensaje` varchar(1000) DEFAULT NULL,
  `registro_id` bigint DEFAULT NULL,
  `creador_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKq6do8ir0rbyyhp5ckh47uq1ny` (`registro_id`),
  KEY `FK4444tqdx2qjwwbqi0dnnx0vr0` (`creador_id`),
  CONSTRAINT `FK4444tqdx2qjwwbqi0dnnx0vr0` FOREIGN KEY (`creador_id`) REFERENCES `Usuario` (`id`),
  CONSTRAINT `FKq6do8ir0rbyyhp5ckh47uq1ny` FOREIGN KEY (`registro_id`) REFERENCES `Peticiones` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Comentarios`
--

LOCK TABLES `Comentarios` WRITE;
/*!40000 ALTER TABLE `Comentarios` DISABLE KEYS */;
INSERT INTO `Comentarios` VALUES (35,NULL,'2020-11-08','20:58:59','Este es un comentario',21,1),(36,NULL,'2020-11-08','20:59:17','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',21,1);
/*!40000 ALTER TABLE `Comentarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Nivel`
--

DROP TABLE IF EXISTS `Nivel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Nivel` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `numero` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Nivel`
--

LOCK TABLES `Nivel` WRITE;
/*!40000 ALTER TABLE `Nivel` DISABLE KEYS */;
INSERT INTO `Nivel` VALUES (1,'Superusuario',1),(2,'Administrador Global',2),(3,'Administrador de Campus',3),(4,'Usuario',4);
/*!40000 ALTER TABLE `Nivel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Peticiones`
--

DROP TABLE IF EXISTS `Peticiones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Peticiones` (
  `id` bigint NOT NULL,
  `activo` bit(1) NOT NULL,
  `contacto` bit(1) NOT NULL,
  `fecha` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `horario` varchar(255) DEFAULT NULL,
  `mensaje` varchar(1000) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `admin_id` bigint DEFAULT NULL,
  `assignee_id` bigint DEFAULT NULL,
  `campus_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKaovpvhla236o81xr7ce913ylg` (`admin_id`),
  KEY `FKiws40jmkgw72ueby9rfmjjupw` (`assignee_id`),
  KEY `FKg4wwxnbnp7ptwe1pd9vvbkq7c` (`campus_id`),
  CONSTRAINT `FKaovpvhla236o81xr7ce913ylg` FOREIGN KEY (`admin_id`) REFERENCES `Usuario` (`id`),
  CONSTRAINT `FKg4wwxnbnp7ptwe1pd9vvbkq7c` FOREIGN KEY (`campus_id`) REFERENCES `Campus` (`id`),
  CONSTRAINT `FKiws40jmkgw72ueby9rfmjjupw` FOREIGN KEY (`assignee_id`) REFERENCES `Usuario` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Peticiones`
--

LOCK TABLES `Peticiones` WRITE;
/*!40000 ALTER TABLE `Peticiones` DISABLE KEYS */;
INSERT INTO `Peticiones` VALUES (18,_binary '',_binary '','2020-11-02','17:45:33','De 5 a 7','Mensaje de prueba 1','Prueba 1','545645231321',NULL,NULL,4),(19,_binary '\0',_binary '\0','2020-11-02','18:13:33','','Prueba numero 2','Anonimo',' ',1,27,4),(21,_binary '',_binary '','2020-11-02','18:15:58','A partir de las 5','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.','Mr H','6141234567',1,25,4),(22,_binary '',_binary '\0','2020-11-02','18:18:02','','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.sadsLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Let','Anonimo','',NULL,NULL,4);
/*!40000 ALTER TABLE `Peticiones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Registros`
--

DROP TABLE IF EXISTS `Registros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Registros` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `cantidad` bigint NOT NULL,
  `fecha` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `log` varchar(255) DEFAULT NULL,
  `admin_id` bigint DEFAULT NULL,
  `hero_id` bigint DEFAULT NULL,
  `activo` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_l37tgr4uywc8y76dybt6fpn2j` (`admin_id`),
  KEY `FK_f8n6o294ju0dlyi838gb3hfdr` (`hero_id`),
  CONSTRAINT `FK_f8n6o294ju0dlyi838gb3hfdr` FOREIGN KEY (`hero_id`) REFERENCES `Usuario` (`id`),
  CONSTRAINT `FK_l37tgr4uywc8y76dybt6fpn2j` FOREIGN KEY (`admin_id`) REFERENCES `Usuario` (`id`),
  CONSTRAINT `FKc5u11i3amtaotml4a8m9pxer6` FOREIGN KEY (`hero_id`) REFERENCES `Usuario` (`id`),
  CONSTRAINT `FKpe5uivfn4s7xehj9awdksyhkx` FOREIGN KEY (`admin_id`) REFERENCES `Usuario` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Registros`
--

LOCK TABLES `Registros` WRITE;
/*!40000 ALTER TABLE `Registros` DISABLE KEYS */;
/*!40000 ALTER TABLE `Registros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Usuario`
--

DROP TABLE IF EXISTS `Usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Usuario` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `apellido` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `campus_id` bigint DEFAULT NULL,
  `nivel_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_60oip607la1lppw1wbynw5la8` (`campus_id`),
  KEY `FK_87kkgmk1jw5ogcawkaq222jlx` (`nivel_id`),
  CONSTRAINT `FK_60oip607la1lppw1wbynw5la8` FOREIGN KEY (`campus_id`) REFERENCES `Campus` (`id`),
  CONSTRAINT `FK_87kkgmk1jw5ogcawkaq222jlx` FOREIGN KEY (`nivel_id`) REFERENCES `Nivel` (`id`),
  CONSTRAINT `FKekvsf94cu2hly3wtievguc3x9` FOREIGN KEY (`campus_id`) REFERENCES `Campus` (`id`),
  CONSTRAINT `FKq9p3bhjh2h2dan9lu2b1b30gv` FOREIGN KEY (`nivel_id`) REFERENCES `Nivel` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Usuario`
--

LOCK TABLES `Usuario` WRITE;
/*!40000 ALTER TABLE `Usuario` DISABLE KEYS */;
INSERT INTO `Usuario` VALUES (1,'Olivas','Hector','c6c9b053f4a00e627f10628d4371688a','heoli',4,1),(24,'Farias','Aldo','69bb23ca275704f46c65d8ed4ac3b5d7','alfar51',4,2),(25,'Escudero','Judith','69bb23ca275704f46c65d8ed4ac3b5d7','juesc57',4,3),(27,'Regular','Usuario','69bb23ca275704f46c65d8ed4ac3b5d7','usreg90',4,4);
/*!40000 ALTER TABLE `Usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (37),(37),(37),(37);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-08 21:16:36
