package models;

import java.sql.Date;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

import play.db.jpa.Model;

@Entity
public class Comentarios extends Model{
	@OneToOne
	public Usuario creador;
	@OneToOne
	public Peticiones registro;
	public Date fecha;
	public Time hora;
	public String mensaje;
	
	
	public Comentarios(Usuario creador, String mensaje, Peticiones registro) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		Calendar cal = Calendar.getInstance();		   
		   
		this.fecha = java.sql.Date.valueOf(dateFormat.format(cal.getTime()).toString());
		this.hora = java.sql.Time.valueOf(timeFormat.format(cal.getTime()).toString());	
		this.creador = creador;
		this.mensaje = mensaje;
		this.registro = registro;
		this.save();
	}
	
	public Comentarios() {
		
	}
	
	

}
