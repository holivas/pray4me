package models;

import java.sql.Date;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import play.db.jpa.Model;

@Entity
public class Peticiones extends Model{
	public Date fecha;
	public Time hora;
	public String mensaje;
	public String nombre;
	public boolean contacto;
	public String telefono;
	public String horario;
	@OneToOne
	public Campus campus;
	@OneToOne
	public Usuario assignee;
	@OneToOne
	public Usuario admin;
	public boolean activo;
	
	public Peticiones(String mensaje, String nombre, Long campus, boolean contacto, String telefono, String horario)
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		Calendar cal = Calendar.getInstance();		   
		   
		this.fecha = java.sql.Date.valueOf(dateFormat.format(cal.getTime()).toString());
		this.hora = java.sql.Time.valueOf(timeFormat.format(cal.getTime()).toString());	
		this.mensaje = mensaje;	
		this.nombre = nombre;
		this.contacto = contacto;
		this.telefono = telefono;
		this.horario = horario;
		this.campus = Campus.findById(campus);
		this.activo = true;		
		this.save();
	}
	
	public void borrarRegistro()
	{
		this.activo = false;
		this.save();
	}
	
	public void asignar(Usuario admin, Usuario assignee) {
		this.admin = admin;
		this.assignee = assignee;
		this.save();
		
	}
}
