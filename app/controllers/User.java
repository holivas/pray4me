package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;

public class User extends Controller {
	
	
	@Before
	static void verificar() {
		String usuario = session.get("usuario");
		if(usuario == null)
		{
			Login.index();
		}
		if(getUser().nivel.numero!=4)
			Login.redirect();
	}
	
	public static Usuario getUser()	{
		Long id = Long.parseLong(session.get("usuario"));
    	Usuario user = Usuario.findById(id);
    	return user;
	}

    public static void index() {
    	Usuario mismo = getUser();
    	List<Usuario> usuarios = Usuario.find("campus = ?1 and nivel.numero > ?2",mismo.campus, mismo.nivel.numero).fetch();
    	render(usuarios);
    }
    
    public static void misPeticiones() {
    	List<Peticiones> peticiones = Peticiones.find("assignee = ?1 and activo = ?2",getUser(), true).fetch();
    	render(peticiones); 
    }
    
    public static void registrosDePeticiones() {
    	List<Peticiones> peticiones = Peticiones.find("assignee = ?1 and activo = ?2",getUser(), true).fetch();
    	render(peticiones);    	
    }
       
   public static void registrosDePeticionesAsignadas() {
    	List<Peticiones> peticiones = Peticiones.find("assignee =?1 and activo = ?2",getUser(),true).fetch();
    	render(peticiones);    	
    }
    
    public static void registrosDePeticionesCompletadas() {
    	List<Peticiones> peticiones = Peticiones.find("activo = ?1 and assignee = ?2",false,getUser()).fetch();
    	render(peticiones);    	
    }
    
    public static void editarUsuario(Long id) {
    	Usuario user = Usuario.findById(getUser().id);
    	render(user);
    }
            
    public static void modificarUsuario(Long id,String nombre, String apellido, String nivel, String campus) {
    	if (id == getUser().id) {
	    	Usuario user = Usuario.findById(id);
	    	user.nombre = nombre;
	    	user.apellido = apellido;
	    	user.nivel = Nivel.find("nombre = ?1", nivel).first();
	    	user.campus = Campus.find("nombre = ?1",campus).first();
	    	user.save();
	    	renderJSON(user);
    	}
    	else
    		renderJSON("Usuario no autorizado");
    }
    
    public static void cambiarPass(Long id, String newpass) {
    	if (id == getUser().id) {
	    	Usuario user = Usuario.findById(id);
	    	user.cambiarContrasena(newpass); 
    	}
    	else
    		renderJSON("Usuario no autorizado");
    	
    }
          
    public static void detalleRegistro(Long id) {
    	Peticiones registro = Peticiones.findById(id);
    	if(registro.assignee == null) {
	    	Usuario usuarioVacio = new Usuario();
	    	usuarioVacio.nombre = "Peticion no asignada";
	    	usuarioVacio.apellido = "";
	    	registro.admin = usuarioVacio;
	    	registro.assignee = usuarioVacio;
    	}
    	List<Comentarios> comentarios = Comentarios.find("registro = ?1",registro).fetch();
    	if (comentarios.isEmpty()) {
    		Comentarios comentario = new Comentarios();
    		Usuario usuarioVacio = new Usuario();
	    	usuarioVacio.nombre = "";
	    	usuarioVacio.apellido = "";
    		comentario.mensaje = "Este registro no contiene comentarios";
    		comentario.creador = usuarioVacio;
    		comentarios.add(comentario);
    	}
    	render (registro,comentarios);
    }
    
    public static void removeRegister(Long id) {
    	Peticiones registro = Peticiones.findById(id);
    	render(registro);
    }
    
    public static void eliminarRegistro(Long id)  {
    	Peticiones registro = Peticiones.findById(id);
    	if(registro.assignee == getUser()) {
	    	registro.activo = false;
	    	registro.save();
    	}
    	renderJSON(registro);
    	
    }
    
    public static void guardarComentario(String comentario, Long registroId) {
    	Peticiones registro = Peticiones.findById(registroId);
    	Usuario creador = getUser();
    	
    	Comentarios nuevoComentario = new Comentarios(creador,comentario,registro);
    	renderJSON(nuevoComentario);
    }
    
    public static void autoEditar() {
    	
    	editarUsuario(getUser().id);
    }
    

}