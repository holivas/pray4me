package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;

public class AdminGlobal extends Controller {
	
	
	@Before
	static void verificar() {
		String usuario = session.get("usuario");
		if(usuario == null)
		{
			Login.index();
		}
		if(getUser().nivel.numero!=2)
			Login.redirect();
	}
	
	public static Usuario getUser()	{
		Long id = Long.parseLong(session.get("usuario"));
    	Usuario user = Usuario.findById(id);
    	return user;
	}

    public static void index() {
    	Usuario mismo = getUser();
    	List<Usuario> usuarios = Usuario.find("nivel.numero > ?1", mismo.nivel.numero).fetch();
    	render(usuarios);
    	render(usuarios);
    }
    
    public static void misPeticiones() {
    	Usuario mismo = getUser();
    	List<Peticiones> peticiones = Peticiones.find("admin = ?1 or assignee = ?2 and activo = ?3 ",mismo,mismo, true).fetch();
    	render(peticiones); 
    }
    public static void crearUsuario()
    {
    	Usuario mismo = getUser();
    	List<Campus> campuses = Campus.findAll();    	
    	List<Nivel> niveles = Nivel.find("numero > ?1",mismo.nivel.numero).fetch();
    	render(campuses,niveles);
    }
    
    public static void registrosDePeticiones() {
    	List<Peticiones> peticiones = Peticiones.find("assignee is null").fetch();
    	render(peticiones);    	
    }
    
    public static void registrosDePeticionesAsignadas() {
    	List<Peticiones> peticiones = Peticiones.find("assignee is not null and activo = ?1",true).fetch();
    	render(peticiones);    	
    }
    
    public static void registrosDePeticionesCompletadas() {
    	List<Peticiones> peticiones = Peticiones.find("activo = ?1",false).fetch();
    	render(peticiones);    	
    }
        
    
    public static void newUser(String nombre, String apellido, String password, int nivel, Long campus)  {
    	Usuario user = new Usuario(nombre,apellido,password,nivel,campus);    	
    	render(user);
    }
    
    public static void removeUser(Long id)
    {
    	Usuario user = Usuario.findById(id);
    	render(user);
    	
    }    
    
    public static void eliminarUsuario(Long id) {
    	Usuario user = Usuario.findById(id);
    	List<Peticiones> reg = Peticiones.find("hero.id = ?1",id).fetch();
    	for(Peticiones r:reg)
    	{
    		r.delete();
    	}
    	user.delete();
    	renderJSON(user);
    }
    
    public static void newCampus(String nombre) {
    	Campus campus = new Campus(nombre);
    	renderJSON(campus);
    }
    
    public static void newLevel(String nombre, int numero) {
    	Nivel nivel = new Nivel(nombre, numero);
    	renderJSON(nivel);
    }
    
    public static void editarUsuario(Long id) {
    	Usuario user = Usuario.findById(id);
    	render(user);
    }
    
        
    public static void modificarUsuario(Long id,String nombre, String apellido, String nivel, String campus) {
    	Usuario user = Usuario.findById(id);
    	user.nombre = nombre;
    	user.apellido = apellido;
    	user.nivel = Nivel.find("nombre = ?1", nivel).first();
    	user.campus = Campus.find("nombre = ?1",campus).first();
    	user.save();
    	renderJSON(user);    	
    }
    
    public static void cambiarPass(Long id, String newpass) {
    	Usuario user = Usuario.findById(id);
    	user.cambiarContrasena(newpass);    	
    	
    }
        
    public static void registros() {
    	List<Peticiones> registros = Peticiones.find("activo = ?1 order by id desc",true).fetch();;
    	render(registros);
    	
    }
    
    public static void detalleRegistro(Long id) {
    	Peticiones registro = Peticiones.findById(id);
    	List<Usuario> usuarios = Usuario.find("campus = ?1", registro.campus).fetch();
    	if(registro.assignee == null) {
	    	Usuario usuarioVacio = new Usuario();
	    	usuarioVacio.nombre = "Peticion no asignada";
	    	usuarioVacio.apellido = "";
	    	registro.admin = usuarioVacio;
	    	registro.assignee = usuarioVacio;
    	}
    	List<Comentarios> comentarios = Comentarios.find("registro = ?1",registro).fetch();
    	if (comentarios.isEmpty()) {
    		Comentarios comentario = new Comentarios();
    		Usuario usuarioVacio = new Usuario();
	    	usuarioVacio.nombre = "";
	    	usuarioVacio.apellido = "";
    		comentario.mensaje = "Este registro no contiene comentarios";
    		comentario.creador = usuarioVacio;
    		comentarios.add(comentario);
    	}
    	render (registro,usuarios,comentarios);
    }
    
    public static void asignarRegistro(Long assignee, Long id) {
    	Peticiones peticion = Peticiones.findById(id);
    	peticion.assignee = Usuario.findById(assignee);
    	peticion.admin = getUser();
    	peticion.save();
    	renderJSON(peticion);    	
    }
    
    public static void removeRegister(Long id) {
    	Peticiones registro = Peticiones.findById(id);
    	render(registro);
    }
    
    public static void eliminarRegistro(Long id)  {
    	Peticiones registro = Peticiones.findById(id);
    	if(registro.assignee == getUser()) {
	    	registro.activo = false;
	    	registro.save();
    	}
    	renderJSON(registro);
    	
    }
    
    public static void guardarComentario(String comentario, Long registroId) {
    	Peticiones registro = Peticiones.findById(registroId);
    	Usuario creador = getUser();
    	
    	Comentarios nuevoComentario = new Comentarios(creador,comentario,registro);
    	renderJSON(nuevoComentario);
    }
    
    public static void autoEditar() {
    	
    	editarUsuario(getUser().id);
    }
    

}