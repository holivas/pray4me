package controllers;

import java.util.List;
import models.*;
import play.mvc.Controller;



public class Application extends Controller{
	static Long chihuahua = (long) 4;//Temporal solo para mostrar Chihuahua
	
    public static void index() {
    	
    	List<Campus> campuses = Campus.findAll(); //Muestra todos los campus
    	//List<Campus> campuses = Campus.find("id = ?1",chihuahua).fetch();//Solo muestra Chihuahua
    	render(campuses);
    }
    
    public static void crearPeticion(String mensaje, String nombre, Long campus, String contacto, String telefono, String horario) {
    	boolean contactoBool;
    	String nombreProcesado;
    	
    	if (new String (contacto).contentEquals("Si"))
    		contactoBool = true;
    	else 
    		contactoBool = false;
    	
    	if (new String (nombre).contentEquals(""))
    		nombreProcesado = "Anonimo";
    	else
    		nombreProcesado = nombre;
    	
    	Peticiones peticion = new Peticiones(mensaje,nombreProcesado,campus,contactoBool,telefono,horario);
    	render(peticion);
    }
}
