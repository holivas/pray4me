# Pray4me
Es una aplicacion para recibir peticiones de oracion y darles seguimiento a traves de un proceso de asignacion y comentarios de seguimiento.

## Administracion
Para iniciar la aplicacion 
```
scripts/pray4me start
```
Para detener la aplicacion 
```
scripts/pray4me stop
```
Para reiniciar la aplicacion 
```
scripts/pray4me restart
```
Para verificar el estado de la aplicacion 
```
scripts/pray4me status
```
Para ver el log de la aplicacion 
```
scripts/pray4me log
```
Para ver el log en vivo de la aplicacion 
```
scripts/pray4me actual
```

